---
title: "Bureau d'Étude 2020"
subtitle: "Licence 3 -- Informatique"
author: "Axel FEFEU"
date: "2020-04-22"
titlepage: true
lang: "fr"
...

# Bureau d'Étude 2020 -- Axel FEFEU

## Sujet proposé

Un générateur de « phrase de passe » dont le système de génération serait basé sur le modèle [$\color{Blue}\text{« xkcd »}$](https://xkcd.com/936/) avec des caractères ajoutés comme fait par le site : [$\color{Blue}\text{xkpasswd}$](https://xkpasswd.net/s/), mais en local.

Lors du lancement du programme sans aucun paramètre un nombre de « phrase de passe » par défaut (par exemple 3) serait générés avec des additions par défaut (par exemple, chaque mot séparé par un « | », avec un nombre à 2 chiffres en préfixe/suffixe, etc…).

L’ajout de paramètre de lancement permettrait une génération suivant une certaine spécification : longueur minimum/maximum des mots utilisés, une casse à respecter, une autre liste de mot à utiliser, etc…
Dans un premier temps, je m’intéresserai à une implémentation « simple » single-threadée puis j’essaierai une implémentation multi-threadée avec les bibliothèques vues lors du semestre impair.
Enfin, une version multi-plateforme ou la création d'une interface graphique pourraient être des axes d'amélioration de ce projet.

Le langage utilisé pour ce sujet serait le C, puisque c'est celui où je me sens le plus confortable, bien que vu les faibles spécifications énoncées ce programme est facilement faisable dans n'importe quel langage permettant une CPRNG (d'ailleurs, il existe déjà une version Python d’un tel programme qui a servi d'inspiration à ce sujet : [$\color{Blue}\text{xkcdpass}$](https://pypi.org/project/xkcdpass/)).