/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: string_manip.h                      *
 * Description: Yet Another XKCD Passphrase  *
 *   Generator                               *
 *   string_manip header                     *
 * Creation date: 2020-05-01                 *
 *********************************************/

#ifndef __STRINGMANIP_H__
#define __STRINGMANIP_H__

/**
 * @brief Function to change a word to lowercase
 *
 * @param word Word to change te case of
 * @return Word lowercased
 */
char *to_lower_case(char *word);

/**
 * @brief Function to change a word to uppercase
 *
 * @param word Word to change te case of
 * @return Word uppercased
 */
char *to_upper_case(char *word);

/**
 * @brief Function to change the case of a word alternatively
 *
 * @param word Word to change te case of
 * @return Word with alternative case
 */
char *to_alternative_case(char *word);
/**
 * @brief Function to change the case of a word randomly
 *
 * @param word Word to change te case of
 * @return Word with a random case
 */
char *to_random_case(char *word);

/**
 * @brief Function to capitalize a word
 *
 * @param word Word to capitalize
 * @return Word capitalized
 */
char *to_capitalize_case(char *word);

/**
 * @brief Function to reverse the case of a word
 *
 * @param word Word to change the case of
 * @return Word with the reversed case
 */
char *to_reverse_case(char *word);
/**
 * @brief Function to change the case of a word
 *
 * @param word Word to change the case of
 * @param case_type Which case to change to
 * @return Word with changed case
 */
char *change_case(char *word, int case_type);
#endif