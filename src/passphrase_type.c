/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: passphrase_type.c                   *
 * Description: Yet Another XKCD Passphrase  *
 *   Generator                               *
 *   passphrase_type definitions             *
 * Creation date: 2020-04-24                 *
 *********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <assert.h>

#include "passphrase_type.h"

/**
 * @brief Maximum length a word can have
 *
 */
#define MAX_WORD_LENGTH 64

typedef struct s_passphrase {
    int nb_words;
    char *prefix;
    char *suffix;
    char *separator;
    char **words;
} Passphrase;

Passphrase *passphrase_create(int max_nb_words) {
    Passphrase *pp = (Passphrase *)malloc(sizeof(struct s_passphrase));
    if (pp == NULL) {
        fprintf(stderr, "passphrase_create: cannot allocate pp\n");
        exit(EXIT_FAILURE);
    }
    pp->nb_words = 0;
    pp->words = (char **)calloc((size_t)max_nb_words, sizeof(char *));
    if (pp->words == NULL) {
        fprintf(stderr, "passphrase_create: cannot allocate pp->words\n");
        exit(EXIT_FAILURE);
    }
    return pp;
}

void passphrase_delete(Passphrase *pp) {
    free(pp->words);
    free(pp);
}

int passphrase_nb_words(Passphrase *pp) {
    return pp->nb_words;
}

void passphrase_set_prefix(Passphrase *pp, char *prefix) {
    pp->prefix = prefix;
}

void passphrase_set_suffix(Passphrase *pp, char *suffix) {
    pp->suffix = suffix;
}

void passphrase_set_separator(Passphrase *pp, char *separator) {
    pp->separator = separator;
}

char *passphrase_prefix(Passphrase *pp) {
    return pp->prefix;
}
char *passphrase_suffix(Passphrase *pp) {
    return pp->suffix;
}
char *passphrase_separator(Passphrase *pp) {
    return pp->separator;
}
char *passphrase_nth_word(Passphrase *pp, int n) {
    assert(0 <= n && n < passphrase_nb_words(pp));
    return pp->words[n];
}

Passphrase *passphrase_add_word(Passphrase *pp, char *word_to_add) {
    pp->words[pp->nb_words] = word_to_add;
    ++pp->nb_words;

    return pp;
}

char *passphrase_toString(Passphrase *pp) {
    size_t len = (size_t)MAX_WORD_LENGTH * (size_t)passphrase_nb_words(pp) * 2;
    char *passphrase_string = (char *)calloc(len, sizeof(char));
    char *prefix = passphrase_prefix(pp);
    char *suffix = passphrase_suffix(pp);
    char *separator = passphrase_separator(pp);
    strcat(passphrase_string, prefix);
    strcat(passphrase_string, separator);
    for (int i = 0; i < passphrase_nb_words(pp); ++i) {
        char *current_word = passphrase_nth_word(pp, i);
        strcat(passphrase_string, current_word);
        strcat(passphrase_string, separator);
    }
    strcat(passphrase_string, suffix);

    return passphrase_string;
}