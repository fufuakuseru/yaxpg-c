/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: string_manip.c                      *
 * Description: Yet Another XKCD Passphrase  *
 *   Generator                               *
 *   string_manip definitions                *
 * Creation date: 2020-05-01                 *
 *********************************************/

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "csprng.h"

/**
 * @brief Constant for lower case
 *
 */
#define LOWER_CASE 0

/**
 * @brief Constant for upper case
 *
 */
#define UPPER_CASE 1

/**
 * @brief Constant for alternative case
 *
 */
#define ALTERNATIVE_CASE 2

/**
 * @brief Constant for random case
 *
 */
#define RANDOM_CASE 3

/**
 * @brief Constant for capitalize case
 *
 */
#define CAPITALIZE_WORD 4

/**
 * @brief Constant for reverse case
 *
 */
#define REVERSE_CASE 5

/**
 * @brief Function to change a word to lowercase
 *
 * @param word Word to change te case of
 * @return Word lowercased
 */
char *to_lower_case(char *word) {
    char *new_word = (char *)calloc(strlen(word), sizeof(char));
    for (unsigned int i = 0; i < strlen(word); ++i) {
        if (isalpha(word[i])) {
            new_word[i] = (char)tolower((int)word[i]);
        } else {
            new_word[i] = word[i];
        }
    }
    return new_word;
}

/**
 * @brief Function to change a word to uppercase
 *
 * @param word Word to change te case of
 * @return Word uppercased
 */
char *to_upper_case(char *word) {
    char *new_word = (char *)calloc(strlen(word), sizeof(char));
    for (unsigned int i = 0; i < strlen(word); ++i) {
        if (isalpha(word[i])) {
            new_word[i] = (char)toupper((int)word[i]);
        } else {
            new_word[i] = word[i];
        }
    }
    return new_word;
}

/**
 * @brief Function to change the case of a word alternatively
 *
 * @param word Word to change te case of
 * @return Word with alternative case
 */
char *to_alternative_case(char *word) {
    char *new_word = (char *)calloc(strlen(word), sizeof(char));
    for (unsigned int i = 0; i < strlen(word); ++i) {
        if (isalpha(word[i])) {
            if (i % 2 == 0) {
                new_word[i] = (char)tolower((int)word[i]);
            } else {
                new_word[i] = (char)toupper((int)word[i]);
            }
        } else {
            new_word[i] = word[i];
        }
    }
    return new_word;
}

/**
 * @brief Function to change the case of a word randomly
 *
 * @param word Word to change te case of
 * @return Word with a random case
 */
char *to_random_case(char *word) {
    char *new_word = (char *)calloc(strlen(word), sizeof(char));
    for (unsigned int i = 0; i < strlen(word); ++i) {
        if (isalpha(word[i])) {
            CSPRNG rng = csprng_create();
            int val = abs((int)csprng_get_int(rng)) % 2;
            if (val) {
                new_word[i] = (char)tolower((int)word[i]);
            } else {
                new_word[i] = (char)toupper((int)word[i]);
            }
            csprng_destroy(rng);
        } else {
            new_word[i] = word[i];
        }
    }
    return new_word;
}

/**
 * @brief Function to capitalize a word
 *
 * @param word Word to capitalize
 * @return Word capitalized
 */
char *to_capitalize_case(char *word) {
    char *new_word = (char *)calloc(strlen(word), sizeof(char));
    for (unsigned int i = 0; i < strlen(word); ++i) {
        if (isalpha(word[i])) {
            if (i == 0) {
                new_word[i] = (char)toupper((int)word[i]);
            } else {
                new_word[i] = (char)tolower((int)word[i]);
            }
        }
    }
    return new_word;
}

/**
 * @brief Function to reverse the case of a word
 *
 * @param word Word to change the case of
 * @return Word with the reversed case
 */
char *to_reverse_case(char *word) {
    char *new_word = (char *)calloc(strlen(word), sizeof(char));
    for (unsigned int i = 0; i < strlen(word); ++i) {
        if (isalpha(word[i])) {
            if (islower(word[i])) {
                new_word[i] = (char)toupper((int)word[i]);
            } else {
                new_word[i] = (char)tolower((int)word[i]);
            }
        } else {
            new_word[i] = word[i];
        }
    }
    return new_word;
}

/**
 * @brief Function to change the case of a word
 *
 * @param word Word to change the case of
 * @param case_type Which case to change to
 * @return Word with changed case
 */
char *change_case(char *word, int case_type) {
    switch (case_type % 6) {
    case LOWER_CASE:
        return to_lower_case(word);
        break;
    case UPPER_CASE:
        return to_upper_case(word);
        break;
    case ALTERNATIVE_CASE:
        return to_alternative_case(word);
        break;
    case RANDOM_CASE:
        return to_random_case(word);
        break;
    case CAPITALIZE_WORD:
        return to_capitalize_case(word);
        break;
    case REVERSE_CASE:
        return to_reverse_case(word);
        break;
    default:
        return to_lower_case(word);
        break;
    }
    return word;
}