/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: wordlist_type.c                     *
 * Description: Yet Another XKCD Passphrase  *
 *   Generator                               *
 *   wordlist_type definitions               *
 * Creation date: 2020-04-23                 *
 *********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "wordlist_type.h"

/**
 * @brief Default space allocation used in the Wordlist
 */
#define DEFAULT_WORDS_ALLOCATION 256

typedef struct s_wordlist {
    int allocation;
    int size;
    char **wordlist;
} Wordlist;

Wordlist *wordlist_create() {
    Wordlist *wl = (Wordlist *)calloc(1, sizeof(struct s_wordlist));
    if (wl == NULL) {
        fprintf(stderr, "wordlist_create: cannot allocate wl\n");
        exit(EXIT_FAILURE);
    }
    wl->allocation = DEFAULT_WORDS_ALLOCATION;
    wl->size = 0;
    wl->wordlist = (char **)calloc((size_t) wl->allocation, sizeof(char *));
    if (wl->wordlist == NULL) {
        fprintf(stderr, "wordlist_create: cannot allocate wl->wordlist\n");
        exit(EXIT_FAILURE);
    }
    return wl;
}

void wordlist_delete(Wordlist *wl) {
    free(wl->wordlist);
    free(wl);
}

int wordlist_allocation(Wordlist *wl) {
    return wl->allocation;
}

int wordlist_size(Wordlist *wl) {
    return wl->size;
}

Wordlist *wordlist_add_word(Wordlist *wl, char *word_to_add) {
    int current_allocation = wordlist_allocation(wl);
    int current_size = wordlist_size(wl);

    if (current_size > current_allocation) {
        wl->allocation += 2;
        wl->wordlist = (char **)realloc(wl->wordlist,
                                        sizeof(char *) * (size_t)wl->allocation);
        if (wl->wordlist == NULL) {
            fprintf(stderr, "wordlist_create: cannot allocate wl->wordlist\n");
            exit(EXIT_FAILURE);
        }
    }

    wl->wordlist[current_size] = word_to_add;
    ++wl->size;

    return wl;
}

char *wordlist_nth(Wordlist *wl, int n) {
    assert(0 <= n && n < wordlist_size(wl));
    return wl->wordlist[n];
}
