/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: yaxpg.c                             *
 * Description: Yet Another XKCD Passphrase  *
 *   Generator                               *
 * Creation date: 2020-04-22                 *
 * Version: 1.86                             *
 *********************************************/

#include <ctype.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "csprng.h"
#include "ini.h"
#include "passphrase_type.h"
#include "string_manip.h"
#include "wordlist_type.h"

/**
 * @brief Current version of the program
 */
#define CURRENT_VERSION "1.86"

/**
 * @brief Maximum length a line in the wordlist file can be
 */
#define MAX_LINE_LENGTH 64

/**
 * @brief Constant for lower case
 */
#define LOWER_CASE 0

/**
 * @brief Constant for upper case
 */
#define UPPER_CASE 1

/**
 * @brief Constant for alternative case
 */
#define ALTERNATIVE_CASE 2

/**
 * @brief Constant for random case
 */
#define RANDOM_CASE 3

/**
 * @brief Constant for capitalize case
 */
#define CAPITALIZE_WORD 4

/**
 * @brief Constant for reverse case
 */
#define REVERSE_CASE 5

/**
 * @brief Flag to print the passphrase during the execution of the program
 */
int verbose_flag = 1;

/**
 * @brief Parameter structure to access the parameters set with the launch options
 */
typedef struct s_parameters {
    char *wordlist_file;
    char *output_file;
    int min_length;
    int max_length;
    int nb_words;
    int case_type;
    int nb_pass;
    int prefix_length;
    int suffix_length;
    int separator_length;
    char *digit_choice;
    char *symbol_choice;
    char *separator;
    char *prefix;
    char *suffix;
    char *config_in;
    char *config_out;
} Parameters;

/**
 * @brief Function to show the usage text
 *
 * @param execname Name of the executable (usually `argv[0]`)
 */
void usage(char* execname) {
    fprintf(stderr, "Usage:\t%s %s %s [OPTIONS]\n",
            execname, "-w WORDLIST_FILE", "[--wordlist=WORDLIST_FILE]");
#define PRINTPARAM(opt_short, opt_long, param) fprintf(stderr, "\t-%s %-21s| --%-s=%s\n", opt_short,param, opt_long, param)
#define PRINTLONGPARAM(opt_long, param) fprintf(stderr, "\t%-24s| --%-s=%s\n", "",opt_long, param)
    PRINTPARAM("m", "min", "MIN_LENGTH");
    PRINTPARAM("M", "max", "MAX_LENGTH");
    PRINTPARAM("n", "nb_words", "NUMBER_OF_WORDS_USED");
    PRINTPARAM("c", "case", "WORD_CASE_TYPE");
    PRINTPARAM("N", "nb_pass", "NUMBER_OF_PASSPHRASES");
    PRINTPARAM("o", "output", "OUTPUT_FILE");
    PRINTPARAM("p", "prefix_length", "PREFIX_LENGTH");
    PRINTPARAM("s", "suffix_length", "SUFFIX_LENGTH");
    PRINTPARAM("S", "separator_length", "SEPARATOR_LENGTH");
    fprintf(stderr, "\t%-24s| --%-s\n", "", "brief");
    PRINTLONGPARAM("set-digit_choice", "DIGIT_CHOICE_STRING");
    PRINTLONGPARAM("set-symbol_choice", "SYMBOL_CHOICE_STRING");
    PRINTLONGPARAM("set-prefix", "PREFIX_STRING");
    PRINTLONGPARAM("set-suffix", "SUFFIX_STRING");
    PRINTLONGPARAM("set-separator", "SEPARATOR_STRING");
    PRINTPARAM("r", "read-config", "CONFIG_INPUT_FILE");
    PRINTLONGPARAM("save-config", "CONFIG_OUTPUT_FILE");

    fprintf(stderr, "\n\tFor the `case` option the possible values are:\n");
    fprintf(stderr, "\t  %-15s %-20s %-28s\n",
            "0: lower case", "1: upper case", "2: alternative case");
    fprintf(stderr, "\t  %-15s %-20s %-28s\n"
            , "3: random case", "4: capitalize case", "5: reverse capitalize case");
    fprintf(stderr,
            "\n\tNote:\n\tFor `prefix_length` (respectively `suffix_length`)"
            "\n\tthe actual affix generated with have PREFIX_LENGTH symbol(s) and"
            "\n\tPREFIX_LENGTH digit(s) (respectively SUFFIX_LENGTH)\n");
    fprintf(stderr,
            "\n\tNote:\n\tIf the `-r` (or `--read-config`) option is used,"
            "\n\tany other launch option(s) already in CONFIG_INPUT_FILE will be"
            "\n\toverwritten by the value in CONFIG_INPUT_FILE\n");

    exit(EXIT_FAILURE);
}

/**
 * @brief Function to print the current version of the program
 */
void version() {
    printf("Yet Another XKCD Passphrase Generator (yaxpg-c) - version %s\n",
           CURRENT_VERSION);
    printf("Copyright (C) 2020 - Axel FEFEU\n\n");
}

/**
 * @brief Function to show the parameters used in the passphrase generation
 *
 * @param parameters Parameters structure
 */
void print_parameters(Parameters parameters) {
    char* case_string = "lower case";
    switch (parameters.case_type % 6) {
    case LOWER_CASE:
        case_string = "lower case";
        break;
    case UPPER_CASE:
        case_string = "upper case";
        break;
    case ALTERNATIVE_CASE:
        case_string = "alternative case";
        break;
    case RANDOM_CASE:
        case_string = "random case";
        break;
    case CAPITALIZE_WORD:
        case_string = "capitalize case";
        break;
    case REVERSE_CASE:
        case_string = "reverse capitalize case";
        break;
    default:
        break;
    }

#define PRINTSTRINGPARAM(s, v) if (v != NULL) printf("\t- %-34s %s\n", s, v)
#define PRINTINTPARAM(s, v) printf("\t- %-34s %d\n", s, v)
#define PRINTPADPARAM(s, v) printf("\t- %-34s %d (%d digit(s), %d symbol(s))\n", s, v*2, v,v)
    printf("Parameters used:\n");
    PRINTSTRINGPARAM("wordlist file:", parameters.wordlist_file);
    PRINTINTPARAM("minimum length of words:", parameters.min_length);
    PRINTINTPARAM("maximum length of words:", parameters.max_length);
    PRINTINTPARAM("number of words used:", parameters.nb_words);
    PRINTSTRINGPARAM("type of case:", case_string);
    PRINTINTPARAM("number of passphrases to generate:", parameters.nb_pass);
    PRINTPADPARAM("prefix length:", parameters.prefix_length);
    PRINTPADPARAM("suffix length:", parameters.suffix_length);
    PRINTINTPARAM("separator length:", parameters.separator_length);
    PRINTSTRINGPARAM("digit string choice:", parameters.digit_choice);
    PRINTSTRINGPARAM("symbol string choice:", parameters.symbol_choice);
    PRINTSTRINGPARAM("output file:", parameters.output_file);
    PRINTSTRINGPARAM("set prefix:", parameters.prefix);
    PRINTSTRINGPARAM("set suffix:", parameters.suffix);
    PRINTSTRINGPARAM("set separator:", parameters.separator);
    PRINTSTRINGPARAM("config in file:", parameters.config_in);
    PRINTSTRINGPARAM("config out file:", parameters.config_out);
}

/**
 * @brief Function to save parameters to a ini file
 *
 * @param parameters Parameters to save
 * @param ini_filename Output file
 */
void save_parameters(Parameters parameters, char *ini_filename) {
    FILE *fileptr = fopen(ini_filename, "w");
    if (fileptr == NULL) {
        perror("save_parameter - fopen");
        exit(EXIT_FAILURE);
    }
#define WRITESTRING(name, value) value == NULL ? fprintf(fileptr, "%s =\n", name) : fprintf(fileptr, "%s = %s\n", name, value)
#define WRITEINT(name, value) fprintf(fileptr, "%s = %d\n", name, value)
    WRITESTRING("wordlist_file", parameters.wordlist_file);
    WRITESTRING("output_file", parameters.output_file);
    WRITEINT("min_length", parameters.min_length);
    WRITEINT("max_length", parameters.max_length);
    WRITEINT("nb_words", parameters.nb_words);
    WRITEINT("case_type", parameters.case_type);
    WRITEINT("nb_pass", parameters.nb_pass);
    WRITEINT("prefix_length", parameters.prefix_length);
    WRITEINT("suffix_length", parameters.suffix_length);
    WRITEINT("separator_length", parameters.separator_length);
    WRITESTRING("digit_choice", parameters.digit_choice);
    WRITESTRING("symbol_choice", parameters.symbol_choice);
    WRITESTRING("separator", parameters.separator);
    WRITESTRING("prefix", parameters.prefix);
    WRITESTRING("suffix", parameters.suffix);
    WRITESTRING("config_in", parameters.config_in);
    WRITESTRING("config_out", parameters.config_out);
    if (!verbose_flag) {
        WRITEINT("brief", 1);
    }
    fclose(fileptr);
}

/**
 * @brief Handler function used by ini_parse
 *
 * @param user Struct to fill
 * @param section Current section of the ini file
 * @param name Current name of the property
 * @param value Current value for the property
 * @return Success or failure
 */
int read_config_handler(void* user, const char* section, const char* name,
                        const char* value) {
    Parameters *parameters = (Parameters *)user;
#define MATCH(n) strcmp(section, "") == 0 && strcmp(name, n) == 0 && strcmp(value, "") != 0

    if (MATCH("wordlist_file")) {
        parameters->wordlist_file = strdup(value);
    } else if (MATCH("output_file")) {
        parameters->output_file = strdup(value);
    } else if (MATCH("min_length")) {
        parameters->min_length = atoi(value);
    } else if (MATCH("max_length")) {
        parameters->max_length = atoi(value);
    } else if (MATCH("nb_words")) {
        parameters->nb_words = atoi(value);
    } else if (MATCH("case_type")) {
        parameters->case_type = atoi(value);
    } else if (MATCH("nb_pass")) {
        parameters->nb_pass = atoi(value);
    } else if (MATCH("prefix_length")) {
        parameters->prefix_length = atoi(value);
    } else if (MATCH("suffix_length")) {
        parameters->suffix_length = atoi(value);
    } else if (MATCH("separator_length")) {
        parameters->separator_length = atoi(value);
    } else if (MATCH("digit_choice")) {
        parameters->digit_choice = strdup(value);
    } else if (MATCH("symbol_choice")) {
        parameters->symbol_choice = strdup(value);
    } else if (MATCH("separator")) {
        parameters->separator = strdup(value);
    } else if (MATCH("prefix")) {
        parameters->prefix = strdup(value);
    } else if (MATCH("suffix")) {
        parameters->suffix = strdup(value);
    } else if (MATCH("config_in")) {
        parameters->config_in = strdup(value);
    } else if (MATCH("config_out")) {
        parameters->config_out = strdup(value);
    } else if (MATCH("brief")) {
        if (value) {
            verbose_flag = 0;
        }
    } else {
        return 0;
    }
    return 1;
}

/**
 * @brief Function that reads a given wordlist file and stores it in a Wordlist
 *
 * @param filename Name of the wordlist file
 * @return Wordlist created from the file.
 */
Wordlist *read_wordlist(char *filename) {
    FILE *fileptr = fopen(filename, "r");
    if (fileptr == NULL) {
        perror("read_wordlist - fopen");
        exit(EXIT_FAILURE);
    }
    Wordlist *wl = wordlist_create();
    for (int i = 0; 1; ++i) {
        char *current_word = malloc(sizeof(char) * MAX_LINE_LENGTH);
        if (current_word == NULL) {
            fprintf(stderr, "read_wordlist: cannot allocate for current_word\n");
            exit(EXIT_FAILURE);
        }
        if (fgets(current_word, MAX_LINE_LENGTH, fileptr) == NULL)
            break;
        current_word[strlen(current_word) - 1] = '\0';
        wordlist_add_word(wl, current_word);
    }
    fclose(fileptr);
    return wl;
}

/**
 * @brief Function that filters the words in a Wordlist to fit a minumum and maximum length
 *
 * @param wl Wordlist to filter
 * @param min_length Minium length for a word
 * @param max_length Maximum length for a word
 * @return Filtered Wordlist
 *
 * @par Axioms
 * @parblock
 * 	\f$\forall\f$`n` : `min_length` \f$\leq\f$ `wordlist_nth(wl, n)` \f$\leq\f$ `max_length`
 * @endparblock
 */
Wordlist *filter_wordlist_length(Wordlist * wl, int min_length,
                                 int max_length) {
    Wordlist *filtered_wl = wordlist_create();
    int wl_size = wordlist_size(wl);
    for (int i = 0; i < wl_size; ++i) {
        char *current_word = wordlist_nth(wl, i);
        int current_word_len = (int)strlen(current_word);
        if (current_word_len >= min_length && current_word_len <= max_length) {
            wordlist_add_word(filtered_wl, current_word);
        }
    }
    return filtered_wl;
}

/**
 * @brief Function that generates a symbol padding string
 *
 * @param symbol_pad_length Length of the symbol padding string
 * @param choice String to choose symbol from
 * @return Symbol padding string with the right length
 */
char *generate_symbol_padding(int symbol_pad_length, char *choice) {
    if (choice == NULL) {
        choice = "!@$^&*-_+=:|~?/.;";
    }
    char *separator = (char *)calloc((size_t)symbol_pad_length, sizeof(char));
    CSPRNG rng = csprng_create();
    size_t choices_len = strlen(choice);
    for (size_t separator_len = strlen(separator);
            (int)separator_len < symbol_pad_length;
            separator_len = strlen(separator)) {
        int random_index = abs((int)csprng_get_int(rng)) % (int)choices_len;
        separator[separator_len] = choice[random_index];
    }
    csprng_destroy(rng);
    return separator;
}

/**
 * @brief Function that generates a digit padding string
 *
 * @param digit_pad_length Length of the digit padding string
 * @param choice String to choose digit from
 * @return Digit padding string with the right length
 */
char *generate_digit_padding(int digit_pad_length, char *choice) {
    if (choice == NULL) {
        choice = "0123456789";
    }
    char *digit_pad = (char *)calloc((size_t)digit_pad_length, sizeof(char));
    CSPRNG rng = csprng_create();
    size_t choices_len = strlen(choice);
    for (size_t digit_pad_len = strlen(digit_pad);
            (int)digit_pad_len < digit_pad_length;
            digit_pad_len = strlen(digit_pad)) {
        int random_index = abs((int)csprng_get_int(rng)) % (int)choices_len;
        digit_pad[digit_pad_len] = choice[random_index];
    }
    csprng_destroy(rng);
    return digit_pad;
}

/**
 * @brief Function that generates an affix string
 *
 * @param pad_length Length of the padding string for the symbol(s) and digit(s)
 * @param is_prefix Are you generating a prefix or a suffix
 * @param digit_choice String to choose digit(s) from
 * @param symbol_choice String to choose symbol(s) from
 * @return Affix string
 */
char *generate_affix(int pad_length, int is_prefix, char *digit_choice,
                     char *symbol_choice) {
    char *affix = (char *)calloc((size_t)pad_length * 2, sizeof(char));
    char *symbol_pad = generate_symbol_padding(pad_length, symbol_choice);
    char *digit_pad = generate_digit_padding(pad_length, digit_choice);
    if (is_prefix == 0) {
        strcpy(affix, symbol_pad);
        strcat(affix, digit_pad);
    } else {
        strcpy(affix, digit_pad);
        strcat(affix, symbol_pad);
    }
    return affix;
}

/**
 * @brief Function that selects a random word from a given Wordlist
 *
 * @param wl Wordlist to search into
 * @return a random word from wl
 */
char *select_random_word(Wordlist * wl) {
    CSPRNG rng = csprng_create();
    int wl_size = wordlist_size(wl);
    int random_index = abs((int)csprng_get_int(rng)) % wl_size;
    char *selected_word = wordlist_nth(wl, random_index % wl_size);
    csprng_destroy(rng);

    return selected_word;
}

/**
 * @brief Function that builds a Passphrase
 *
 * @param wl Wordlist to use to choose the words
 * @param max_nb_words Number of words from the Wordlist that will from the passphrase
 * @param prefix Prefix of the passphrase
 * @param suffix Suffix of the passphrase
 * @param separator Seperator use in the passphrase
 * @param case_type Case used for words
 * @return Generated Passphrase
 */
Passphrase *build_passphrase(Wordlist * wl, int max_nb_words, char *prefix,
                             char *suffix, char *separator, int case_type) {
    Passphrase *pp = passphrase_create(max_nb_words);
    passphrase_set_prefix(pp, prefix);
    passphrase_set_suffix(pp, suffix);
    passphrase_set_separator(pp, separator);
    for (int i = 0; i < max_nb_words; ++i) {
        char *random_word = select_random_word(wl);
        char *changed_cased_word = change_case(random_word, case_type);
        passphrase_add_word(pp, changed_cased_word);
    }

    return pp;
}

/**
 * @brief Function that returns the number of digits in an Integer
 *
 * @param n Integer to process
 * @return Number of digits in the Integer
 */
int nb_digits (int n) {
    int current = n;
    int len = 0;
    while (current != 0) {
        current /= 10;
        ++len;
    }
    return len;
}

/**
 * @brief Function to generate passphrase(s)
 *
 * @param parameters Parameters structure
 */
void generate_passphrases(Parameters parameters) {
    Wordlist* wl = read_wordlist(parameters.wordlist_file);
    Wordlist* filtered_wl = filter_wordlist_length(wl, parameters.min_length,
                            parameters.max_length);

    FILE *fileptr;
    if (parameters.output_file != NULL) {
        fileptr = fopen(parameters.output_file, "w+");
        if (fileptr == NULL) {
            perror("read_wordlist - fopen");
            exit(EXIT_FAILURE);
        }
    }

    Passphrase *pp_tab[parameters.nb_pass];

    if (verbose_flag) {
        if (parameters.nb_pass > 1) {
            printf("\nGenerated passphrases:\n\n");
        } else {
            printf("\nGenerated passphrase:\n\n");
        }
    }

    for (int i = 0; i < parameters.nb_pass; ++i) {
        char *prefix;
        char *suffix;
        char *separator;

        prefix = parameters.prefix != NULL ? parameters.prefix :
                 generate_affix(parameters.prefix_length, 1,
                                parameters.digit_choice,
                                parameters.symbol_choice);
        suffix = parameters.suffix != NULL ? parameters.suffix :
                 generate_affix(parameters.suffix_length, 0,
                                parameters.digit_choice,
                                parameters.symbol_choice);

        separator = parameters.separator != NULL ? parameters.separator :
                    generate_symbol_padding(parameters.separator_length,
                                            parameters.symbol_choice);

        pp_tab[i] = build_passphrase(filtered_wl, parameters.nb_words, prefix,
                                     suffix, separator, parameters.case_type);
        char *pp_string = passphrase_toString(pp_tab[i]);
        if (verbose_flag) {
            int format_len = nb_digits(parameters.nb_pass);
            size_t pp_string_len = strlen(pp_string);
            printf("Passphrase %0*d (length %ld):\t%s\n", format_len, i + 1,
                   pp_string_len, pp_string);
        }
        if (parameters.output_file != NULL) {
            fputs(pp_string, fileptr);
            fputs("\n", fileptr);
        }
    }

    if (parameters.output_file != NULL) {
        fclose(fileptr);
    }
    wordlist_delete(wl);
    wordlist_delete(filtered_wl);

    for (int i = 0; i < parameters.nb_pass; ++i) {
        passphrase_delete(pp_tab[i]);
    }
}

int main(int argc, char **argv) {
    Parameters parameters;
    parameters.wordlist_file = NULL;
    parameters.output_file = NULL;
    parameters.min_length = 5;
    parameters.max_length = 9;
    parameters.nb_words = 4;
    CSPRNG rng = csprng_create();
    parameters.case_type = abs((int)csprng_get_int(rng)) % 6;
    csprng_destroy(rng);
    parameters.nb_pass = 5;
    parameters.prefix_length = 2;
    parameters.suffix_length = 2;
    parameters.separator_length = 1;
    parameters.digit_choice = "0123456789";
    parameters.symbol_choice = "!@$^&*-_+=:|~?/.;";
    parameters.prefix = NULL;
    parameters.suffix = NULL;
    parameters.separator = NULL;
    parameters.config_in = NULL;
    parameters.config_out = NULL;

    int opt;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"wordlist", required_argument, 0, 'w'},                   /*  0 */
            {"min",      required_argument, 0, 'm'},                   /*  1 */
            {"max",      required_argument, 0, 'M'},                   /*  2 */
            {"nb_words",   required_argument, 0, 'n'},                 /*  3 */
            {"case",     required_argument, 0, 'c'},                   /*  4 */
            {"nb_pass",     required_argument, 0, 'N'},                /*  5 */
            {"help",     no_argument, 0, 'h'},                         /*  6 */
            {"output", required_argument, 0, 'o'},                     /*  7 */
            {"version", no_argument, 0, 'v'},                          /*  8 */
            {"brief", no_argument, &verbose_flag, 0},                  /*  9 */
            {"prefix_length", required_argument, 0, 'p'},              /* 10 */
            {"suffix_length", required_argument, 0, 's'},              /* 11 */
            {"separator_length", required_argument, 0, 'S'},           /* 12 */
            {"set-digit_choice", required_argument, 0, 0},             /* 13 */
            {"set-symbol_choice", required_argument, 0, 0},            /* 14 */
            {"set-prefix", required_argument, 0, 0},                   /* 15 */
            {"set-suffix", required_argument, 0, 0},                   /* 16 */
            {"set-separator", required_argument, 0, 0},                /* 17 */
            {"read-config", required_argument, 0, 'r'},                /* 18 */
            {"save-config", required_argument, 0, 0},                  /* 19 */
            {0, 0, 0, 0}
        };
        opt = getopt_long(argc, argv, "w:m:M:n:c:N:o:hvbp:s:S:r:", long_options,
                          &option_index);
        if (opt == -1) {
            break;
        }

        int new_min;
        int new_max;
        int new_nb_words;
        int new_case_type;
        int new_nb_pass;
        int new_prefix_length;
        int new_suffix_length;
        int new_separator_length;

        switch (opt) {
        case 'w':
            parameters.wordlist_file = strdup(optarg);
            break;
        case 'm':
            new_min = (int) strtoul(optarg, NULL, 10);
            if (new_min < 0) {
                fprintf(stderr, "\nParameter -m incorrect:\n");
                fprintf(stderr, "min_length (%d) < 0\n", new_min);
                new_min = 5;
                fprintf(stderr, "min_length set to its default value (%d)\n", new_min);
            } else if (new_min > parameters.max_length) {
                fprintf(stderr, "\nParameter -m incorrect:\n");
                fprintf(stderr, "min_length (%d) > max_length (%d)\n", new_min,
                        parameters.max_length);
                new_min = 5;
                fprintf(stderr, "min_length set to its default value (%d)\n", new_min);
            }
            parameters.min_length = new_min;
            break;
        case 'M':
            new_max = (int) strtoul(optarg, NULL, 10);
            if (new_max < 0) {
                fprintf(stderr, "\nParameter -M incorrect:\n");
                fprintf(stderr, "max_length (%d) < 0\n", new_max);
                new_max = 9;
                fprintf(stderr, "max_length set to its default value (%d)\n", new_max);
            } else if (new_max < parameters.min_length) {
                fprintf(stderr, "\nParameter -M incorrect:\n");
                fprintf(stderr, "max_length (%d) < min_length (%d)\n", new_max,
                        parameters.min_length);
                new_max = 9;
                fprintf(stderr, "max_length set to its default value (%d)\n", new_max);
            }
            parameters.max_length = new_max;
            break;
        case 'n':
            new_nb_words = (int) strtoul(optarg, NULL, 10);
            if (new_nb_words < 0) {
                fprintf(stderr, "\nParameter -n incorrect:\n");
                fprintf(stderr, "nb_words (%d) < 0\n", new_nb_words);
                new_nb_words = 4;
                fprintf(stderr, "nb_words set to its default value (%d)\n", new_nb_words);
            }
            parameters.nb_words = new_nb_words;
            break;
        case 'c':
            new_case_type = (int) strtoul(optarg, NULL, 10);
            if (new_case_type < 0) {
                fprintf(stderr, "\nParameter -c incorrect:\n");
                fprintf(stderr, "case_type (%d) < 0\n", new_case_type);
                rng = csprng_create();
                new_case_type = abs((int)csprng_get_int(rng)) % 6;
                csprng_destroy(rng);
                fprintf(stderr, "case_type set to a new random value (%d)\n", new_case_type);
            }
            parameters.case_type = new_case_type;
            break;
        case 'N':
            new_nb_pass = (int) strtoul(optarg, NULL, 10);
            if (new_nb_pass < 0) {
                fprintf(stderr, "\nParameter -N incorrect:\n");
                fprintf(stderr, "nb_pass (%d) < 0\n", new_nb_pass);
                new_nb_pass = 5;
                fprintf(stderr, "nb_pass set to its default value (%d)\n", new_nb_pass);
            }
            parameters.nb_pass = new_nb_pass;
            break;
        case 'o':
            parameters.output_file = strdup(optarg);
            break;
        case 'h':
            usage(argv[0]);
            break;
        case 'v':
            version();
            break;
        case 'b':
            verbose_flag = 0;
            break;
        case 'p':
            new_prefix_length = (int) strtoul(optarg, NULL, 10);
            if (new_prefix_length < 0) {
                fprintf(stderr, "\nParameter -p incorrect:\n");
                fprintf(stderr, "new_prefix_length (%d) < 0\n",
                        new_prefix_length);
                new_prefix_length = 2;
                fprintf(stderr, "new_prefix_length set to its default value (%d)\n",
                        new_prefix_length);
            }
            parameters.prefix_length = new_prefix_length;
            break;
        case 's':
            new_suffix_length = (int) strtoul(optarg, NULL, 10);
            if (new_suffix_length < 0) {
                fprintf(stderr, "\nParameter -s incorrect:\n");
                fprintf(stderr, "new_suffix_length (%d) < 0\n", new_suffix_length);
                new_suffix_length = 2;
                fprintf(stderr, "new_suffix_length set to its default value (%d)\n",
                        new_suffix_length);
            }
            parameters.suffix_length = new_suffix_length;
            break;
        case 'S':
            new_separator_length = (int) strtoul(optarg, NULL, 10);
            if (new_separator_length < 0) {
                fprintf(stderr, "\nParameter -S incorrect:\n");
                fprintf(stderr, "new_separator_length (%d) < 0\n",
                        new_separator_length);
                new_separator_length = 1;
                fprintf(stderr, "new_separator_length set to its default value (%d)\n",
                        new_separator_length);
            }
            parameters.separator_length = new_separator_length;
            break;
        case 'r':
            parameters.config_in = strdup(optarg);
            break;
        case 0:
            switch (option_index) {
            case 13:
                parameters.digit_choice = strdup(optarg);
                break;
            case 14:
                parameters.symbol_choice = strdup(optarg);
                break;
            case 15:
                parameters.prefix = strdup(optarg);
                break;
            case 16:
                parameters.suffix = strdup(optarg);
                break;
            case 17:
                parameters.separator = strdup(optarg);
                break;
            case 19:
                parameters.config_out = strdup(optarg);
                break;
            }
            break;
        default:
            usage(argv[0]);
            break;
        }
    }

    if (parameters.config_in != NULL) {
        if (ini_parse(parameters.config_in, read_config_handler, &parameters) < 0) {
            fprintf(stderr, "Cannot load %s\n", parameters.config_in);
            return EXIT_FAILURE;
        }
    }

    if (parameters.wordlist_file == NULL) {
        usage(argv[0]);
    }

    printf("\n");

    if (parameters.config_out != NULL) {
        save_parameters(parameters, parameters.config_out);
    }

    print_parameters(parameters);
    generate_passphrases(parameters);

    return 0;
}