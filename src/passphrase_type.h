/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: passphrase_type.h                   *
 * Description: Yet Another XKCD Passphrase  *
 *   Generator                               *
 *   passphrase_type header                  *
 * Creation date: 2020-04-23                 *
 *********************************************/

#ifndef __PASSPHRASE_H__
#define __PASSPHRASE_H__

/**
 * @brief Opaque definition of the Passphrase abstract data type.
 */
typedef struct s_passphrase Passphrase;

/**
 * @brief Constructor of an empty Passphrase.
 *
 * @param max_nb_words Maximum number of words used in the Passphrase
 * @return Correctly initialized Passphrase.
 *
 * @par Profile
 * @parblock
 * 	passphrase_create : Integer \f$\rightarrow\f$ Passphrase
 * @endparblock
 */
Passphrase *passphrase_create(int max_nb_words);

/**
 * @brief Destructor of a Passphrase
 *
 * @param pp Passphrase to delete
 *
 * @par Profile
 * @parblock
 * 	passphrase_delete : Passphrase \f$\rightarrow\f$
 * @endparblock
 */
void passphrase_delete(Passphrase *pp);

/**
 * @brief Access the number of words in the Passphrase
 *
 * @param pp Passphrase to access
 *
 * @par Profile
 * @parblock
 *  passphrase_nb_words : Passphrase \f$\rightarrow\f$ Integer
 * @endparblock
 *
 * @par Axioms
 * @parblock
 *  `passphrase_nb_words(passphrase_create(x))` \f$= 0\f$ \n
 *  `passphrase_nb_words(passphrase_add_words(pp, w))` \f$=\f$
 * `passphrase_nb_words(pp)` \f$+ 1\f$ \n
 * @endparblock
 */
int passphrase_nb_words(Passphrase *pp);

/**
 * @brief Setter for a Passphrase's prefix
 *
 * @param pp Passphrase to access
 * @param prefix Prefix to set to
 *
 * @par Profile
 * @parblock
 * 	passphrase_set_prefix : Passphrase \f$\times\f$ String \f$\rightarrow\f$
 * Passphrase
 * @endparblock
 */
void passphrase_set_prefix(Passphrase *pp, char *prefix);

/**
 * @brief Setter for a Passphrase's suffix
 *
 * @param pp Passphrase to access
 * @param suffix Suffix to set to
 *
 * @par Profile
 * @parblock
 * 	passphrase_set_prefix : Passphrase \f$\times\f$ String \f$\rightarrow\f$
 * Passphrase
 * @endparblock
 */
void passphrase_set_suffix(Passphrase *pp, char *suffix);

/**
 * @brief Setter for a Passphrase's separator
 *
 * @param pp Passphrase to access
 * @param separator Separator to set to
 *
 * @par Profile
 * @parblock
 * 	passphrase_set_prefix : Passphrase \f$\times\f$ String \f$\rightarrow\f$
 * Passphrase
 * @endparblock
 */
void passphrase_set_separator(Passphrase *pp, char *separator);

/**
 * @brief Getter for a Passphrase's prefix
 *
 * @param pp Passphrase to access
 *
 * @par Profile
 * @parblock
 * 	passphrase_prefix : Passphrase \f$\rightarrow\f$ String
 * @endparblock
 */
char *passphrase_prefix(Passphrase *pp);

/**
 * @brief Getter for a Passphrase's suffix
 *
 * @param pp Passphrase to access
 *
 * @par Profile
 * @parblock
 * 	passphrase_suffix : Passphrase \f$\rightarrow\f$ String
 * @endparblock
 */
char *passphrase_suffix(Passphrase *pp);

/**
 * @brief Getter for a Passphrase's separator
 *
 * @param pp Passphrase to access
 *
 * @par Profile
 * @parblock
 * 	passphrase_separator : Passphrase \f$\rightarrow\f$ String
 * @endparblock
 */
char *passphrase_separator(Passphrase *pp);

/**
 * @brief Getter for a Passphrase's nth word
 *
 * @param pp Passphrase to access
 * @param n Index of the word to access
 *
 * @par Profile
 * @parblock
 * 	passphrase_nth_word : Passphrase \f$\times\f$ Integer \f$\rightarrow\f$
 * String
 * @endparblock
 *
 * @pre
 * 	\f$0 \le n <\f$ `passphrase_nb_words(pp)`
 */
char *passphrase_nth_word(Passphrase *pp, int n);

/**
 * @brief Add a word to a Passphrase
 *
 * @param pp Passphrase to add into
 * @param word_to_add Word to add in
 * @return Modified Passphrase
 *
 * @par Profile
 * @parblock
 * 	passphrase_set_prefix : Passphrase \f$\times\f$ String \f$\rightarrow\f$
 * Passphrase
 * @endparblock
 */
Passphrase *passphrase_add_word(Passphrase *pp, char *word_to_add);

/**
 * @brief toString for a Passphrase
 *
 * @param pp Passphrase to transform
 * @return Passphrase as a string
 *
 * @par Profile
 * @parblock
 * 	passphrase_toString : Passphrase \f$\rightarrow\f$ String
 * @endparblock
 */
char *passphrase_toString(Passphrase *pp);

#endif