/*********************************************
 *   ___     ______ ___________ _____ _   _  *
 *  / _ \    |  ___|  ___|  ___|  ___| | | | *
 * / /_\ \   | |_  | |__ | |_  | |__ | | | | *
 * |  _  |   |  _| |  __||  _| |  __|| | | | *
 * | | | |_  | |   | |___| |   | |___| |_| | *
 * \_| |_(_) \_|   \____/\_|   \____/ \___/  *
 *                                           *
 *                                           *
 * File: wordlist_type.h                     *
 * Description: Yet Another XKCD Passphrase  *
 *   Generator                               *
 *   wordlist_type header                    *
 * Creation date: 2020-04-22                 *
 *********************************************/

#ifndef __WORDLIST_H__
#define __WORDLIST_H__

/**
 * @brief Opaque definition of the Wordlist abstract data type.
 */
typedef struct s_wordlist Wordlist;

/**
 * @brief Constructor of an empty Wordlist.
 *
 * @return Correctly initialized Wordlist.
 *
 * @par Profile
 * @parblock
 * 	wordlist_create : \f$\rightarrow\f$ Wordlist
 * @endparblock
 */
Wordlist *wordlist_create();

/**
 * @brief Destructor of a Wordlist
 *
 * @param wl Wordlist to delete.
 *
 * @par Profile
 * @parblock
 *	wordlist_delete : Wordlist \f$\rightarrow\f$
 * @endparblock
 */
void wordlist_delete(Wordlist *wl);

/**
 * @brief Access to the size of the Wordlist.
 *
 * @param wl Wordlist to access
 * @return Number of elements in the Wordlist.
 *
 * @par Profile
 * @parblock
 * 	wordlist_size : Wordlist \f$\rightarrow\f$ Integer
 * @endparblock
 *
 * @par Axioms
 * @parblock
 * 	`wordlist_size(wordlist_create())` \f$= 0\f$ \n
 * 	`wordlist_size(wordlist_add_word(wl, w))` \f$=\f$
 * `wordlist_size(wl)` \f$+ 1\f$
 * @endparblock
 */
int wordlist_size(Wordlist *wl);

/**
 * @brief Add a word to a Wordlist.
 *
 * @param wl Worldlist to add into
 * @param word_to_add Word to add in
 * @return Modified Wordlist
 *
 * @par Profile
 * @parblock
 * 	wordlist_add_word : Wordlist \f$\times\f$ String \f$\rightarrow\f$
 * Wordlist
 * @endparblock
 */
Wordlist *wordlist_add_word(Wordlist *wl, char *word_to_add);

/**
 * @brief Access to the \f$n^{th}\f$ word of a Wordlist.
 *
 * @param wl Wordlist to access
 * @param n Index of the required word
 * @return \f$n^{th}\f$ word of the Wordlist.
 *
 * @par Profile
 * @parblock
 * 	wordlist_nth : Wordlist \f$\times\f$ Integer \f$\rightarrow\f$
 *String
 * @endparblock
 *
 * @pre
 * 	\f$0 \le n <\f$ `wordlist_size(wl)`
 */
char *wordlist_nth(Wordlist *wl, int n);

#endif