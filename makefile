CC      = gcc
CFLAGS  = -Wall -Wextra -Wconversion
LDFLAGS =
SRC     = $(wildcard src/*.c)
OBJ     = $(SRC:.c=.o)
EXE     = $(subst .c,,$(SRC))


ifeq ($(DEBUG),yes)
	CFLAGS += -g
	LDFLAGS +=
else
	CFLAGS += -O2 -DNDEBUG
	LDFLAGS +=
endif

all: yaxpg-c
everything: pdf yaxpg-c

%.o: %.c
	@printf "\t%-7s: %-18s → %s\n" "CC" $< $@
	@$(CC) -o $@ -c $< $(CFLAGS)

yaxpg-c: $(OBJ)
	@printf "\t%-7s: %-18s → %s\n" "CCLD" $< $@
	@$(CC) -o $@ $^ $(LDFLAGS)
	@if [ ! -d obj ];\
	then printf "\t%-7s: %-18s\n" "MKDIR" "obj";\
	mkdir -p obj;\
	fi
	@mv src/*.o obj/

clean:
	@if [ -d obj ];\
	then printf "\t%-7s: %-18s\n" "RM" "obj/";\
	rm -rf obj/* obj;\
	fi
	@if [ -f .gdb_history ];\
	then printf "\t%-7s: %-18s\n" "RM" ".gdb_history";rm -rf .gdb_history;\
	fi
	@if [ -f out.txt ];\
	then printf "\t%-7s: %-18s\n" "RM" "out.txt";\
	rm -rf out.txt;\
	fi

cleandoc:
	@if [ -d doc ];\
	then printf "\t%-7s: %-18s\n" "RM" "doc/";\
	rm -rf doc/* doc;\
	fi

mrproper: clean cleandoc
	@if [ -f yaxpg-c ];\
	then printf "\t%-7s: %-18s\n" "RM" "yaxpg-c";\
	rm -rf yaxpg-c;\
	fi

rebuild: mrproper all

doc: cleandoc
	@printf "\t%-7s: %-18s → %s\n" "doxygen" "doxyfile" "doc/"
	@doxygen doxyfile > /dev/null

doclatex: doc
	@cd doc/latex && make

pdf:
	@printf "\t%-7s: %-18s → %s\n" "pandoc" "sujet.md" "sujet.pdf"
	@pandoc --pdf-engine=xelatex --template custom-eisvogel --highlight-style ~/.pandoc/highlight-style/custom-kate.theme sujet.md -o sujet.pdf

test: yaxpg-c clean
	./yaxpg-c -w wordlist_english

testparam: yaxpg-c clean
	./yaxpg-c -w wordlist_english -m 3 -M 8 -n 2 -c 0 -N 16 -o out.txt -b --set-digit_choice="0" --set-symbol_choice="()" --set-prefix="×" --set-suffix="÷" --set-separator="-" --save-config=test.ini

testnegparam: yaxpg-c clean
	./yaxpg-c -w wordlist_english -m -3 -M -8 -n -2 -c 0 -N -16

rebuild: mrproper all

.PHONY: all clean mrproper rebuild
