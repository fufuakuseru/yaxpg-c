# Yet Another XKCD Passphrase Generator in C – yaxpg-c

Generate [xkcd](https://xkcd.com/936/)-style passphrase like [xkpasswd](https://xkpasswd.net/s/) but locally using C.

# How to use

Clone this repository with:

```sh
git clone https://framagit.org/fufuakuseru/yaxpg.git
```

Inside the cloned repository compile the program with:

```sh
make
```

To get the usage text of the program launch with `--help` or `-h`:

```sh
./yaxpg-c --help
```

It'll show you that you need to have at least one parameter which is a path to a wordlist file (that only uses ASCII characters), some are provided in this repository ([wordlist_english](./wordlist_english), [wordlist_french](./wordlist_french), [wordlist_latin](./wordlist_latin)). So to make the program work simply launch it with the path to a wordlist file as the first parameter. Exemple:

```sh
./yaxpg-c -w wordlist_english
```

Alternatively you can also make use of a ini file to set your parameters and launch the program with the `-r` option. For instance using the [exemple.ini](./exemple.ini) provided:

```sh
./yaxpg-c -r exemple.ini
```

To generate the html/latex documentations use:

```sh
make doc
```

You can remove all the compilation files with:

```sh
make clean
```

You can remove all the documentation files with:

```sh
make cleandoc
```

# Checklist

- [x] Read wordlist
- [x] Store appropriate words
- [x] Select random words
- [x] Concat words with separator
- [x] Concat passphrase with affixes
- [x] Display passphrase
- [x] Generate multiple passphrases
- [x] Change cases of words
- [x] Manage launch options
- [x] Save config to ini file
- [x] Read ini config file

## For v2

- ~~Multi-threaded generation of multiple passphrases~~
    - Rudimentary tests of a multi-threaded implementation resulted in no gain of speed (need to research why, moved to a potential v2.5)
- [ ] Make a lightweight UI

## For v2.5

- [ ] Multi-threaded generation of multiple passphrases

## For v3

- [ ] Port it to Windows


# Uses

- [https://github.com/Duthomhas/CSPRNG](https://github.com/Duthomhas/CSPRNG)
- [https://github.com/benhoyt/inih](https://github.com/benhoyt/inih)